$(function(){
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: true,
        autoplay: true,
        delay: 1000,
        // 如果需要分页器
        pagination: {
          el: '.swiper-pagination',
        },
        
        // 如果需要前进后退按钮
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        
      })
      
      setNews();
      setProduct();
      getTap();
      setTop();
      sethuo();
})



//新闻部分
function setNews(){
  var newArr = $(".news-content");
  $(".news-list span").on("click",function(){
      $(this).css({background:"#29a4ff"}).siblings().css({background:"#5c5c5c"});
      var index = $(this).index();
      $(newArr[index]).siblings(".news-content").hide();
      $(newArr[index]).fadeIn();
  })
}

//产品内容页
function setProduct(){
  var presentpath = window.location.pathname;
  $(".trade-navigation-mark > a").each(function(index,vel){
      var url = $(vel).attr('data-url');
      if(presentpath === url) $(vel).addClass('active');
  })
}

function getTap(){
  var subnav = $(".header-list-max");
  $("li[data-menu='nav']").on("mouseover",function(){
          var index = $(this).index();
          var maxHeight=$(subnav[index-1]).height();
          if(index === 0){
              $(this).parent().parent().parent().siblings(".header-list").height(0);
          }else{
              $(this).parent().parent().parent().siblings(".header-list").height(maxHeight+80);
          }
          if((navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE9.0") || navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE8.0"){
              for(var i = 0; i < subnav.length;i++){
                  subnav[i].style.display = "none";
              }
          }
          var Aleft = $(this).width();
          $(this).siblings("span").width(Aleft)
          $(this).siblings("span").show().stop().animate({
              "left":160+(index-1)*(Aleft+62)+"px"
          },600,"swing");
          if((navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE9.0") ||navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE8.0"){
              $(subnav[index-1]).show();
          }
          $(subnav[index-1]).siblings().removeClass("open");
          $(subnav[index-1]).addClass("open");
          
           
         
  }).on("mouseout",function(){
          var index = $(this).index();
          if(index === 0){
              $(this).siblings("span").hide();
          }
  })
  $(".header-list").on("mouseleave",function(){
      $(this).height(0);
      $("#header-sp").hide();
  })
  
}

function setTop(){
  $(window).scroll(function(){
      if($(document).scrollTop() > 144){
         $(".header").children(".header-top").fadeOut();
         $(".header").addClass("header-sty");
      }
      if($(document).scrollTop() <= 0){
          $(".header").children(".header-top").fadeIn();
          $(".header").removeClass("header-sty");
      }
  })
}
function sethuo(){
  $(window).scroll(function(){
      if($(document).scrollTop() > 200){
          $(".topTmg").fadeIn(500);
      }else{
          $(".topTmg").fadeOut(500);
      }
  });
  
  $(".topTmg").on("click",function(){
      $('body,html').animate({scrollTop:0},1000); 
      return false; 
  })
}

